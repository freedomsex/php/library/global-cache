<?php

namespace FreedomSex\Services;

use Enqueue\Dsn\Dsn;

class GlobalCache
{
    use CounterStorageTrait;

    const CACHE_PREFIX = 'global_cache';
    const CACHE_RANDOM = '5oHGXK9NemOo';

    private $memcached;

    public function __construct(string $memcachedDsn)
    {
        $dsn = Dsn::parseFirst($memcachedDsn);
        $this->memcached = new \Memcached();
        $this->memcached->setOption(\Memcached::OPT_BINARY_PROTOCOL, true);
        $this->memcached->addServer($dsn->getHost(), $dsn->getPort());
    }

    public function getInstance(): \Memcached
    {
        return $this->memcached;
    }

    public function prefix() {
        return self::CACHE_PREFIX . "_" . self::CACHE_RANDOM . "__";
    }

    public function delete($key)
    {
        $prefixedKey = $this->prefix() . $key;
        return $this->memcached->delete($prefixedKey);
    }

    public function load($key, $default = null)
    {
        $prefixedKey = $this->prefix() . $key;
        return $this->memcached->get($prefixedKey) ?? $default;
    }

    public function touch($key, int $expiry = 0)
    {
        $prefixedKey = $this->prefix() . $key;
        return $this->memcached->touch($prefixedKey, $expiry);
    }

    public function save($key, $value, $expires = 0)
    {
        $prefixedKey = $this->prefix() . $key;
        return $this->memcached->set($prefixedKey, $value, $expires);
    }

}
