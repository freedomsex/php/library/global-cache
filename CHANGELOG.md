# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.1](https://gitlab.com/freedomsex/php/library/global-cache/compare/1.0.0...1.0.1) (2022-10-02)


### Bug Fixes

* dsn ([5659ef1](https://gitlab.com/freedomsex/php/library/global-cache/commit/5659ef178bfb502bf69b9e0246bb7162749f5492))

## 1.0.0 (2022-10-02)
